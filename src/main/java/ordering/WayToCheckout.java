package ordering;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WayToCheckout {

    @FindBy(xpath = " //div[text()=\"Футболка Оversize\"]")
    private WebElement Tshirt;
    @FindBy(xpath = "//td[@class=\"js-store-prod-popup-buy-btn-txt\"]")
    private WebElement addToBasket;
    @FindBy(css = "svg.t706__carticon-img")
    private WebElement buttonBasket;
    @FindBy(xpath = " //button[@class=\"t706__sidebar-continue t-btn\"]")
    private WebElement buttonCheckout;



    private WebDriver driver;
    public WayToCheckout(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }


    public WayToCheckout oversize(){
        Tshirt.click();
        return this;
    }

    public WayToCheckout Basket(){
        addToBasket.click();
        return this;
    }

    public WayToCheckout basketButton(){
        buttonBasket.click();
        return this;
    }

    public WayToCheckout checkoutButton(){
        buttonCheckout.click();
        return this;
    }
}
