package ordering;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UserDataPage {

    @FindBy(xpath = "//input[@id=\"input_1496239431201\"]")
    private WebElement nameField;
    @FindBy(id = "input_1627385047591")
    private WebElement townField;
    @FindBy(xpath = "//input[@style=\"color: rgb(94, 94, 94);\"]")
    private WebElement telField;
    @FindBy(id = "input_1630305196291")
    private WebElement addressField;
    @FindBy(xpath = "//label[@data-service-id=\"1444328482\"]")
    private WebElement checkbox;
    @FindBy(xpath = "//input[@name=\"tildadelivery-userinitials\"]")
    private WebElement recipientField;
    @FindBy(xpath = "//input[@name=\"tildadelivery-aptoffice\"]")
    private WebElement flatField;
    @FindBy(xpath = "//div[@class=\"t-form__submit\"]/button[text()=\"ОФОРМИТЬ ЗАКАЗ\"]")
    private WebElement button;
    //Assertions
    @FindBy(xpath = "//div[@class=\"t-form__errorbox-middle\"]//p[@style=\"display: block;\"]")
    WebElement aboutButton;
    @FindBy(xpath = "//div[@id=\"error_1496239478607\"]")
    WebElement aboutTel;


    private WebDriver driver;
    public UserDataPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public UserDataPage setName(String name){
        nameField.sendKeys(name);
        return this;
    }

    public UserDataPage setTown(String town){
        townField.sendKeys(town);
        return this;
    }

    public UserDataPage tel(){
        telField.click();
        return this;
    }

    public UserDataPage setTel(String tele){
        telField.sendKeys(tele);
        return this;
    }

    public UserDataPage setAddress(String address){
        addressField.sendKeys(address);
        return this;
    }

    public UserDataPage checkBoxChoice(){
        checkbox.click();
        return this;
    }

    public UserDataPage setRecipient(String recipient){
        recipientField.sendKeys(recipient);
        return this;
    }

    public UserDataPage setFlat(String flat){
        flatField.sendKeys(flat);
        return this;
    }

    public UserDataPage setButton(){
        button.click();
        return this;
    }

    public String Assertions1(){
        return aboutButton.getText();
    }

    public String Assertions2(){
        return aboutButton.getText();
    }

}
