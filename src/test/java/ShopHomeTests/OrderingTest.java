package ShopHomeTests;

import ordering.UserDataPage;
import ordering.WayToCheckout;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;


public class OrderingTest {
    private WebDriver driver;
    private UserDataPage userData;
    private WayToCheckout wayToCheckout;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "/Users/klukina9013/Downloads/untitled5/chromedriver_mac64/chromedriver");
        ChromeOptions options = new ChromeOptions();
        driver = new ChromeDriver();
        userData = new UserDataPage(driver);
        wayToCheckout = new WayToCheckout(driver);
    }
    @After
    public void tearDown(){
        driver.quit();
    }
    @Test
    public void testSecond() {
        SoftAssertions softAssertions = new SoftAssertions();

        //1. Открыть ссылку https://homebrandofficial.ru/wear
        driver.get("https://homebrandofficial.ru/wear");
        driver.manage().window().fullscreen();

        //2. Нажать на товар с названием “Футболка Оversize”
        wayToCheckout.oversize();

        //3. Нажать на кнопку “Добавить в корзину”
        wayToCheckout.Basket();

        //4. Нажать на иконку "Корзина"
        wayToCheckout.basketButton();

        //5. Нажать "Оформить заказ"
        wayToCheckout.checkoutButton();

        //6. Заполнить все поля рандомными данными
        //6.1. ФИО полностью
        String userName = "Клюкина Валерия Петровна";
        userData.setName(userName);

        //6.2. Телефон
        String telephone = "0000000000";
        userData.tel();
        userData.setTel(telephone);

        //6.3. Карта региона или страны
        String userTown = "Москва";
        userData.setTown(userTown);

        //6.4.Адрес для доставки
        String userAddress = "Москва, ул. Московская, д.1, кв.1";
        userData.setAddress(userAddress);

        //7.Доставка
        //7.1.Город
        //7.2.Почта России (Доставка по РФ)
        userData.checkBoxChoice();

        //7.3.Получатель ФИО
        userData.setRecipient(userName);

        //7.4.Улица
        WebElement dropdownField = driver.findElement(By.xpath("//input[@name=\"tildadelivery-street\"]"));
        dropdownField.sendKeys("Московская");
        List<WebElement> dropdownList = new WebDriverWait(driver, 3)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector(".searchbox-list-item")));
        dropdownList.stream()
                .filter(element -> element.findElement(By.cssSelector(".searchbox-list-item-text"))
                        .getText().contains("ул Московская"))
                .filter(element -> element.findElement(By.cssSelector(".searchbox-list-item-description"))
                        .getText().contains("г Москва, Россия"))
                .findFirst()    .orElseThrow(()->new AssertionError("No element found by this criteria"))
                .click();

        //7.5. Дом
        WebElement dropdownField2 = driver.findElement(By.xpath("//input[@name=\"tildadelivery-house\"]"));
        dropdownField2.sendKeys("1");
        List<WebElement> dropdownList2 = new WebDriverWait(driver, 3)
                .until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@data-name=\"д. 1\"]")));
        dropdownList2.stream()
                .filter(element -> element.findElement(By.xpath("//div[@data-name=\"д. 1\"]/div[1]"))
                        .getText().contains("д. 1"))
                .findFirst()    .orElseThrow(()->new AssertionError("No element found by this criteria"))
                .click();

        //7.6.Квартира/офис
        String flatNumber = "1";
        userData.setFlat(flatNumber);

        //8. Нажать кнопку “Оформить заказ”
        userData.setButton();

        //проверки
        String attention = "Укажите, пожалуйста, корректный номер телефона";

        softAssertions.assertThat(userData.Assertions1()).as("Неверно указано (или отсутствует) предупреждение около кнопки ОФОРМИТЬ ЗАКАЗ")
                .isEqualToIgnoringCase(attention);
        softAssertions.assertThat(userData.Assertions2()).as("Неверно указано (или отсутствует) предупреждение около поля ТЕЛЕФОН")
                .isEqualToIgnoringCase(attention);
        softAssertions.assertAll();
    }
}
